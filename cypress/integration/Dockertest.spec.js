/// <reference types=" Cypress"/>
describe('fill the form test',()=>{
    it('Add task',()=>{   
        cy.visit('http://localhost/')
        cy.get('[href="/task/add"]').click()
        cy.get('#summary').click()
        .type('add test')
        cy.get('#detail').click()
        .type('Test')
        cy.get('#priority')
        .select('Low')
        cy.get('#duedate')
        .type('2019-12-01')
        cy.get('.btn').click()
    })

    it('Add Edit',()=>{   
      
        cy.visit('http://localhost/')
        cy.get('[href="/task/add"]').click()
        cy.get('#summary').click()
        .type('add test2')
        cy.get('#detail').click()
        .type('Test edit')
        cy.get('#priority')
        .select('High')
        cy.get('#duedate')
        .type('2019-10-01')
        cy.get('.btn').click()



        cy.get(':nth-child(1) > .nav-link > .fas').click()
        cy.get('#summary').clear()
        cy.get('#summary').click()
        .type('edit test')
        cy.get('#detail').clear()
        cy.get('#detail').click()
        .type('Test edit2')
        cy.get('#priority')
        .select('Normal')
        cy.get('#duedate')
        .type('2019-04-06')
        cy.get('.btn').click()

    })
        it('Delete task',()=>{   
        cy.visit('http://localhost/')
        cy.get('[href="/task/add"]').click()
        cy.get('#summary').click()
        .type('delete test')
        cy.get('#detail').click()
        .type('Test delete')
        cy.get('#priority')
        .select('Low')
        cy.get('#duedate')
        .type('2019-12-01')
        cy.get('.btn').click()


        cy.get('[cl=""] > .nav-link > .fas').click()
        cy.get('.btn-danger').click()
    })


    it('Delete task and click cancal',()=>{   
        cy.visit('http://localhost/')
        cy.get('[href="/task/add"]').click()
        cy.get('#summary').click()
        .type('delete test')
        cy.get('#detail').click()
        .type('Test delete and test cancel')
        cy.get('#priority')
        .select('Low')
        cy.get('#duedate')
        .type('2019-12-01')
        cy.get('.btn').click()


        cy.get(':nth-child(1) > .nav-link > .fas').click()
        cy.get('.btn-primary').click()
    })

        it('Complete task',()=>{   
        cy.visit('http://localhost/')
        cy.get('[href="/task/add"]').click()
        cy.get('#summary').click()
        .type('complete test')
        cy.get('#detail').click()
        .type('Test complete ')
        cy.get('#priority')
        .select('High')
        cy.get('#duedate')
        .type('2019-12-08')

        cy.get('.btn').click()

        cy.get('body > div > div > div.card-footer > div > div:nth-child(2) > ul > li:nth-child(3)').click()

    })


})